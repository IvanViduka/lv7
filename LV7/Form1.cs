﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV7
{
    public partial class Form1 : Form
    {
        bool trenutni_igrac = true; //true prvi igrac, false drugi
        int count = 0;
        string ime_prvog = "";
        string ime_drugog = "";
        int draw = 0;
        int win1 = 0;
        int win2 = 0;

        private void Pobjednik() {

            bool win = false;

            if ((btn_1.Text == btn_2.Text) && (btn_2.Text == btn_3.Text) && (!btn_1.Enabled))
            {
                win = true;
            }

            else if ((btn_4.Text == btn_5.Text) && (btn_5.Text == btn_6.Text) && (!btn_4.Enabled))
            {
                win = true;
            }

            else if ((btn_9.Text == btn_8.Text) && (btn_8.Text == btn_9.Text) && (!btn_7.Enabled))
            {
                win = true;
            }



            else if ((btn_1.Text == btn_4.Text) && (btn_4.Text == btn_7.Text) && (!btn_1.Enabled))
            {
                win = true;
            }

            else if ((btn_2.Text == btn_5.Text) && (btn_5.Text == btn_8.Text) && (!btn_2.Enabled))
            {
                win = true;
            }

            else if ((btn_3.Text == btn_6.Text) && (btn_6.Text == btn_9.Text) && (!btn_3.Enabled))
            {
                win = true;
            }


            else if ((btn_1.Text == btn_5.Text) && (btn_5.Text == btn_9.Text) && (!btn_1.Enabled))
            {
                win = true;
            }

            else if ((btn_3.Text == btn_5.Text) && (btn_5.Text == btn_7.Text) && (!btn_4.Enabled))
            {
                win = true;
            }

            

            if (win)
            {


                if (trenutni_igrac)
                {
                    MessageBox.Show(ime_prvog + " je pobjednik", "Kraj");
                    win1++;
                }

                else
                {
                    MessageBox.Show(ime_drugog + " je pobjednik", "Kraj");
                    win2++;
                }
            }

            else
            {
                if (count == 9)
                {
                    MessageBox.Show("Nerijeseno", "Kraj");
                    draw++;
                }

            }
        }

       


        public Form1()
        {
            InitializeComponent();
        }

        private void button_click(object sender, EventArgs e)
        {
            
            Button b = (Button)sender;
            if (trenutni_igrac)
            {
                b.Text = "X";
                
                lbl_trenutni.Text = ime_drugog;
            }

            else
            {
                b.Text = "O";
                
                lbl_trenutni.Text = ime_prvog;
            }

            b.Enabled = false;
            count++;
            Pobjednik();
            trenutni_igrac = !trenutni_igrac;

        }

        private void btn_igra_Click(object sender, EventArgs e)
        {
            trenutni_igrac = true;
            count = 0;
            ime_prvog = txt_box_first.Text;
            ime_drugog = txt_box_second.Text;
            txt_box_first.Text = "";
            txt_box_second.Text = "";
            lbl_trenutni.Text = ime_prvog;
            lbl_win1.Text = win1.ToString();
            lbl_win2.Text = win2.ToString();
            lbl_draw.Text = draw.ToString();

            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";

                }
            }

            catch { }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
